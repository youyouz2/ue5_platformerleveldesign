# Platformer Level Design Documentation

Developed with Unreal Engine 5.


## Map Design
Start – Control point – Gate – End
Control point led to 4 other areas for exploration
Control point and 4 areas have enemies
## Enemy Design
- Pursuer
- Stand boss
- Creative boss
    - 刺客？

## Main Player Design
- Control
    - WASD=前后左右
    - space=jump
    - left=attack
    - mouse scroll=switch backpack item
    - (pending) right=shield
        - if attacker touches shield, still deduct damage but lesser amount
    - F=interact

### Downloaded Asset Original Link
- https://itch.io/game-assets/free/tag-unreal-engine
- https://devilsworkshop.itch.io/lowpoly-forest-pack
- https://devilsworkshop.itch.io/low-poly-3d-hex-pack-free
- https://fertile-soil-productions.itch.io/modular-village-pack


---
## Note (to myself for development)
- Project settings – Engine Input – Action Mappings
    - InteractAlt – E – another type of interaction
    - Interact – 左键 – main interact


## Log
- Day 1: Figured out how to add an animation from Mixamo to the existing character from Unreal Learning Asset. Now player can use WASD, space for jump, left mouse click for attact, mouse scroll up or down to switch backpack item. Added HUD including health bar and backpack w/ 4 slots.
- Day 2: Found many assets for map design. Also copied part of learning assets building to serve as the final checkpoint building.
